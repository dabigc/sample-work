# 👋 Hey there, I'm Cole!

I'm a software engineer in the Chicago area.  
I went to college for multimedia design back when CSS was new, Dreamweaver was the IDE of choice and 
Flash websites were the norm. I can still make my way around Photoshop and Illustrator and find joy in 
it when the opportunity presents itself.  

Ultimately I found a passion for infrastructure and have been honing my skills in this space for over a decade.  
I'm constantly learning and evolving to keep up with what's new.  
I currently spend a lot of my time with Kubernetes, HashiCorp tooling, AWS, Bash and Python.  
I'm building my comprehension with Go and am tempted by Rust.

This repo is acting as somewhat of a portfolio containing work samples that I've been the primary contributor to.  
The samples in this repo are in no way reflective of the entirety of my skill set. Sorry about that.  
As most of my work has been for organizations, I can only cherry-pick generic enough examples to share.

I'm continuing to add to this repo so please check back if it looks light to you.

Thanks for giving these samples a look! If you'd like to connect, I'd love to hear from you 
via [email](mailto:cole@coursecode.net) or [LinkedIn](https://www.linkedin.com/in/dabigc/).