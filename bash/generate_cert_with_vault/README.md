README
======

This script is intended to be a general use certificate script for one-off
certificate creation to be used with manual updates for certs in the few cases
where we haven't automated renewal.

Pre-requisites
==============

- [jq](https://stedolan.github.io/jq/) (<=1.6) 
- [Vault](https://www.vaultproject.io/downloads) (match version of cluster
  you're targeting)
- [wget](https://stackoverflow.com/questions/33886917/how-to-install-wget-in-macos) (<=1.21.2)


Variables
=========

- `-p` - The name of the PKI mount in the Vault cluster that you want to
  interact with to generate the certificate.
- `-r` - The [Vault
  role](https://www.vaultproject.io/api-docs/secret/pki#generate-certificate-and-key)
  used in the `vault write` command to generate the certificate with.
- `-n` - The [common
  name](https://www.vaultproject.io/api-docs/secret/pki#common_name) used on the
  certificate.
- `-i` (optional) - The [IP SANs](https://www.vaultproject.io/api-docs/secret/pki#ip_sans) added to the certificate if declared. Default is empty.

Usage
=====

1. Login to the Vault cluster you need to interact with based on what
you're updating certs for.

2. Run this script with the arguments.

3. The certificates can then be found in the directory that the script creates.
   This directory name will match the common name that was passed into the script.

Example
-------

This example would generate a certificate on the <REDACTED> PKI
mount in the Vault cluster that could be used on the front-end of Vault for
interactions with the cluster.

```
./generate_cert_with_vault.sh -p [PKI_MOUNT_NAME] -r [ROLE_NAME] -n [VAULT_CLUSTER_ADDRESS] -i 127.0.0.1
```