#!/bin/bash
#Description: Generate TLS / SSL Certificates for a Vault PKI

#Pre-Requisites:
######## 1. You have Vault Credentials setup in your session for the Vault cluster.
######## 2. jq package installed.

# Parameters
######## -p pki_int_path  (PKI path in Vault)
######## -r pki_int_role  (PKI role name in Vault)
######## -n common_name   (common name to assign to the certificate)
######## [-i cert_ip_sans] OPTIONAL - alternative ip_sans in single comma delimited string, otherwise defaults to ""
########      e.g  192.168.0.1,127.0.0.1

set -e

usage() { echo "Usage: $0 [-p <string>] [-r <string>] [-n <string>] [ (optional) -i <string>]" 1>&2; exit 1; }

while getopts ":p:r:n:i:" opt; do
    case "${opt}" in
        p)  pki_int_path=${OPTARG}  ;;
        r)  pki_int_role=${OPTARG}  ;;
        n)  common_name=${OPTARG} ;;
        i)  cert_ip_sans=${OPTARG}  ;;
        *)  usage;;
    esac
done

shift "$((OPTIND-1))"

if [ -z "${pki_int_path}" ] || [ -z "${pki_int_role}" ] || [ -z "${common_name}" ] ; then
    usage
elif [ -z "${cert_ip_sans}" ] ; then
    cert_ip_sans=""
fi


PKI_INT_PATH=${pki_int_path}
PKI_INT_ROLE=${pki_int_role}
CERT_CN=${common_name}
CERT_IP_SANS=${cert_ip_sans}
CERT_TTL=31536000 #1 year

mkdir -p $PWD/${CERT_CN}
cd $PWD/${CERT_CN} || (printf "Cant change to $PWD/${CERT_CN} \n"; exit 1)

CERT_DATA=$(vault write -format=json ${PKI_INT_PATH}/issue/${PKI_INT_ROLE} common_name=${CERT_CN} ttl=${CERT_TTL} ip_sans=${CERT_IP_SANS})

printf "Vault Certificate generated: \n"

printf ">> Storing objects to $PWD \n"
printf "Saving Intermediate CA to intermediate_ca.pem \n"

printf "%s" "${CERT_DATA}" | jq -r ".data.ca_chain[0]" >> intermediate_ca.pem
printf "Saving CERTIFICATE to certificate.crt \n"
printf "%s" "${CERT_DATA}" | jq -r ".data.certificate" >> certificate.crt
printf "Saving ISSUING CA to issuing_ca.pem \n"
printf "%s" "${CERT_DATA}" | jq -r ".data.issuing_ca" >> issuing_ca.pem
printf "Saving PRIVATE KEY to private_key.key \n"
printf "%s" "${CERT_DATA}" | jq -r ".data.private_key" >> private_key.key


printf ">> Verifying certificate. If this fails, ensure the logic below isn't leading to the wrong CA being pulled for validation. \n"
if [[ "${PKI_INT_PATH}" == *"dev"* ]]; then
     wget -q https://<REDACTED_DEV_VAULT_CLUSTER_URL>/v1/ca/ca/pem --no-check-certificate --output-document ca.pem
else
    wget -q https://<REDACTED_VAULT_CLUSTER_URL>/v1/ca/ca/pem --no-check-certificate --output-document ca.pem
fi
openssl verify -CAfile ca.pem -untrusted intermediate_ca.pem certificate.crt

printf ">> Generating Full Chain in fullchain.pem \n"
cat certificate.crt intermediate_ca.pem > fullchain.pem
printf "Exiting now."