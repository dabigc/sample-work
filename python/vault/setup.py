from os import path
from setuptools import setup

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md')) as f:
    long_description = f.read()

setup(
    name='find_root_tokens',
    version='0.0.1',
    description='Finds root tokens in a Vault cluster and lists their details (hopefully so you can revoke them).',
    longdescription=long_description,
    longdescription_content_type='text/markdown',
    url='https://gitlab.com/dabigc/sample-work/-/tree/main/python/vault',
    packages=['find_root_tokens'],
    entry_points={
        'console_scripts': [
            'find-root-tokens=find_root_tokens.findroottokens:main',
        ]
    },
    install_requires=[
        'hvac',
        'prettytable',
    ]
)