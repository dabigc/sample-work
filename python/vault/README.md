## [find-root-tokens.py](https://gitlab.com/dabigc/sample-work/-/tree/main/python/vault)
### Use case
This script iterates through the token accessors to find root tokens.
Once a Vault cluster is initialized, root tokens should be revoked as is
[best practice](https://www.vaultproject.io/docs/concepts/tokens.html#root-tokens).
If we're sticking to our agreed upon principal of least privilege for Vault management, 
this script should always come back empty.

### Prerequisites
- The VAULT_TOKEN environment variable must be set with a token that has
  permissions to the `auth/token/accessors` endpoint. Those in the
  <REDACTED> AD group should have these permissions on their
  tokens when logging in via the root namespace.
- The VAULT_ADDR environment variable should be set for the proper Vault
  instance you're targeting.
- The VAULT_NAMESPACE environment variable should not be set or should
  be set to the root namespace as root tokens can only reside in the
  root namespace.
- Tested on Python 2.7.16 and 3.6.10

### Installation
Clone this repo and install via pip from the `python/vault` folder
```
pip install find_root_tokens/
```

### Usage
Once installed and pre-requisites are met, run the tool.
```bash
find-root-tokens
```

### Uninstallation
pip uninstall find-root-tokens