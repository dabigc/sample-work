#!/usr/local/bin/python3

import os
import time
import hvac
import urllib3
import sys
from prettytable import PrettyTable

urllib3.disable_warnings()

def check_environment_variables():
    try:
        os.environ["VAULT_ADDR"]
    except Exception:
        print("The VAULT_ADDR environment must be set.")
        os._exit(1)

    try:
        os.environ["VAULT_TOKEN"]
    except Exception:
        print("The VAULT_TOKEN environment must be set.")
        os._exit(1)

def find_root_tokens():
    client = hvac.Client(url=os.environ['VAULT_ADDR'], verify=False, token=os.environ["VAULT_TOKEN"])
    accessors = client.list('auth/token/accessors')['data']['keys']
    root_token_table = PrettyTable()
    root_token_table.field_names = ["Display Name", "Creation Time", "Expiration Time", "Policies", "Token Accessor"]

    for accessor in accessors:
        output = client.lookup_token(accessor, accessor=True)
        display_name = output['data']['display_name']
        creation_date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(output['data']['creation_time']))
        expire_time = output['data']['expire_time']
        policies = output['data']['policies']
        if "root" in policies:
            root_token_table.add_row([display_name, creation_date, expire_time, policies, accessor])
    print(root_token_table)

def main():
    check_environment_variables()
    find_root_tokens()

if __name__ == "__main__":
    sys.exit(main() or 0)