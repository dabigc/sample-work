import argparse
from arnparse import arnparse
import boto3
import os
import logging
from datetime import datetime, timedelta
from botocore.exceptions import ClientError


def compile_excluded_image_list(already_tagged_images, boto3_sessions, consumer_regions, excluded_instance_states, minimum_age):
	# Obtain images that aren't in use, aren't already tagged
	def iterate_through_accounts(autoscaling_client, ec2_client, excluded_images, excluded_instance_states):
		excluded_instances = ec2_client.describe_instances(
			Filters=[{'Name': 'instance-state-name', 'Values': excluded_instance_states}])
		launch_configurations = autoscaling_client.describe_launch_configurations()['LaunchConfigurations']
		for instance in excluded_instances['Reservations']:
			excluded_images.add(instance['Instances'][0]['ImageId'])

		for image in launch_configurations:
			excluded_images.add(image['ImageId'])

		return excluded_images

	excluded_images = set()

	# Append images that are already tagged to the excluded_images set.
	for image in already_tagged_images:
		excluded_images.add(image['ImageId'])

	# Append images that are older than what's defined in the MINIMUM_AGE attribute to the excluded_images set.
	minimum_age_date = datetime.today() - timedelta(days=minimum_age)
	for image in all_available_images:
		image_creation_date = image['CreationDate']
		formatted_image_creation_date = datetime.strptime(image_creation_date, "%Y-%m-%dT%H:%M:%S.%fZ")
		if minimum_age_date < formatted_image_creation_date:
			excluded_images.add(image['ImageId'])

	# Append images in use in accounts to the excluded_images set.
	for session in boto3_sessions:
		for region in consumer_regions:
			ec2_client = session.client('ec2', region)
			autoscaling_client = session.client('autoscaling', region)
			iterate_through_accounts(autoscaling_client, ec2_client, excluded_images, excluded_instance_states)
	return excluded_images


def tag_eligible_images(all_available_images, already_tagged_images, boto3_sessions, consumer_regions, dryrunbool,
						excluded_instance_states, grace_period, minimum_age, minimum_images,
						primary_account_ec2_client):
	# Obtain images that aren't in use, aren't already tagged and are older than what's defined in the MINIMUM_AGE
	# attribute
	if len(all_available_images) > minimum_images:
		eligible_images = set()
		excluded_images = compile_excluded_image_list(already_tagged_images, boto3_sessions, consumer_regions, excluded_instance_states,
													  minimum_age)

		for image in all_available_images:
			eligible_images.add(image['ImageId'])
		list_of_images_to_be_tagged = list(eligible_images - excluded_images)
		expiration_date = datetime.today() + timedelta(days=grace_period)
		expiration_tag = [{'Key': 'Expires', 'Value': str(expiration_date.date())}]

		if not list_of_images_to_be_tagged:
			logger.info("There are no eligible images to tag as expired. Moving on.")
		else:
			logger.info("There are {0} images to be tagged with Expires tag. Beginning tagging...".format(
				len(list_of_images_to_be_tagged)))

			# Splitting large lists into 500 item sublists to stay well below the 1,000 resource ID per call constraint
			# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html?highlight=describe_instance#EC2.Client.create_tags
			max_list_length = int(500)
			list_of_images_to_be_tagged = [list_of_images_to_be_tagged[i * max_list_length:(i + 1) * max_list_length]
										   for i in range(
					(len(list_of_images_to_be_tagged) + max_list_length - 1) // max_list_length)]

			if len(list_of_images_to_be_tagged) > 1:
				logger.info(
					"List of eligible images is more than allowed in a single call. Batching the operation into groups of "
					"{0}.".format(
						max_list_length))

			for sublist in list_of_images_to_be_tagged:
				try:
					primary_account_ec2_client.create_tags(DryRun=dryrunbool, Resources=sublist, Tags=expiration_tag)
					logger.info(
						"Adding tag of Expires that's set to {0} was successful for the following images: {1}".format(
							expiration_date.date(), sublist))
				except ClientError as e:
					if e.response['Error']['Code'] == 'DryRunOperation':
						logger.info("DryRunOperation succeeded for tagging of images: {0}".format(sublist))
					else:
						logger.error("Failed to tag images: {0}. Please investigate. Error: {1}".format(sublist, e))
						raise
	else:
		logger.info("Minimum number of images required in the defined region of this account is not met. Skipping "
					"tagging of images.")


def is_candidate_image(image, expiration_date):
	# Validate the tag key and value before identifying as candidate for purge
	image_id = image['ImageId']
	if 'Tags' in image:
		for tags in image['Tags']:
			if tags["Key"] == 'Expires':
				logger.info("Expires tag located for image: " + str(image_id))
				try:
					image_datetime = datetime.strptime(tags["Value"], "%Y-%m-%d").date()
					logger.info("image_datetime is:" + str(image_datetime))
				except:
					logger.error("Expires tag value does not match required format for image: " + str(image_id))
					return False
				if image_datetime < expiration_date:
					logger.info("Identified expired image: " + str(image_id))
					return True
	return False


def purge_image(dryrunbool, imageid, primary_account_ec2_client):
	# purge the AMI ensuring we cater for dryrun
	try:
		primary_account_ec2_client.deregister_image(DryRun=dryrunbool, ImageId=imageid)
		logger.info("Deregistered image: {0} ".format(imageid))
	except ClientError as e:
		if e.response['Error']['Code'] == 'DryRunOperation':
			logger.info("DryRunOperation succeeded for deregister of image: {0}".format(imageid))
			return True
		else:
			logger.error("Failed to deregister image: {0}. Error: {1}".format(imageid, e))
			raise
	return True


def purge_snapshots(dryrunbool, images_purge_list, primary_account_ec2_client):
	# purge all images identified and return any failed snapshot purges
	# any failure of an image purge shall be raised as a top level error and halt the Jenkins job
	failed_snapshots = []
	purged_image = False
	for image in images_purge_list:
		# purge image op moved to separate function in order to ensure we can catch dryrun errors
		# without skipping images/snapshots in loop
		purged_image = purge_image(dryrunbool, image['ImageId'], primary_account_ec2_client)
		if purged_image == True:
			for block_device in image['BlockDeviceMappings']:
				# skip non-EBS volumes
				if block_device.get('Ebs', None) is not None:
					try:
						primary_account_ec2_client.delete_snapshot(DryRun=dryrunbool,
																   SnapshotId=block_device['Ebs']['SnapshotId'])
						logger.info("Deleted snapshot: {0} for image: {1}".format(block_device['Ebs']['SnapshotId'],
																				  image['ImageId']))
					except ClientError as e:
						if e.response['Error']['Code'] == 'DryRunOperation':
							logger.info("DryRunOperation succeeded for delete of snapshot: {0} for image: {1}".format(
								block_device['Ebs']['SnapshotId'], image['ImageId']))
						else:
							failed_snapshots.append(block_device['Ebs']['SnapshotId'])
							logger.error("Failed to delete snapshot: {0} for image: {1}. Error: {2}".format(
								block_device['Ebs']['SnapshotId'], image['ImageId'], e))
	return failed_snapshots


def lifecycle_ami(all_available_images, already_tagged_images, boto3_sessions, dryrunbool, minimum_images,
				  primary_account_ec2_client):
	all_available_images = len(all_available_images)
	sts_client = boto3_sessions[0].client('sts')
	aws_account_id = sts_client.get_caller_identity().get('Account')
	aws_region = primary_account_ec2_client.meta.region_name
	expiration_date = datetime.now().date()
	logger.info("Date used for comparison is: " + str(expiration_date))
	logger.info("Locating candidate AMIs based on Expires tag value before: " + str(expiration_date))
	logger.info("Tagged Images: " + str(already_tagged_images))
	images_purge_list = []

	for image in already_tagged_images:
		if all_available_images <= minimum_images:
			break
		if is_candidate_image(image, expiration_date):
			images_purge_list.append(image)
			all_available_images = all_available_images - 1

	logger.info("Purge list: " + str(images_purge_list))
	failed_snapshots = purge_snapshots(dryrunbool, images_purge_list, primary_account_ec2_client)
	msg = ("Account: " + aws_account_id + "\n" + aws_region + ":" + str(os.environ['JOB_NAME']) + ":")
	if not failed_snapshots:
		msg = msg + ("AMI Lifecycle images purged: " + str(len(images_purge_list)) + ": No further action required")
	else:
		msg = msg + ("Failed to purge snapshots: " + str(failed_snapshots) + ": Please investigate.")

	logger.info(msg)


if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument('--additional_account_roles', nargs='+', type=str, help='Additional roles for accounts with '
																				'permission to AMIs in the primary account. '
																				'Used to check for instances or launch '
																				'configurations using AMIs from the source '
																				'AWS account to ensure they\'re excluded from '
																				'tagging or purging', default=[])
	parser.add_argument('--consumer_regions', nargs='+', type=str, default=['us-east-1'], help='List of regions with '
																							   'instances or launch '
																							   'configurations using '
																							   'AMIs from the primary '
																							   'account.')
	parser.add_argument('--dryrun', type=str.lower, default='true',
						help='Sets dry run flag for testing. Set to true if testing. Set to false when ready to '
							 'actually apply changes.')
	parser.add_argument('--excluded_instance_states', nargs='+', type=str,
						default=['pending', 'running', 'shutting-down', 'stopping', 'stopped'],
						help='List of instance states that should be used to exclude tagging an AMI with the expired '
							 'tag.')
	parser.add_argument('--grace_period', type=int, default=30,
						help='Defines the date to be applied in the Expires tag when setting it.')
	parser.add_argument('--image_region', type=str, default='us-east-1', help='Sets the region where AMIs are housed '
																			  'for the primary account ec2 client.')
	parser.add_argument('--log_level', type=int, default=20, help='Sets the log level for the script.')
	parser.add_argument('--minimum_age', type=int, default=30,
						help='Defines how old an image must be in days (from today\'s date) before it\'s tagged with '
							 'the Expires tag.')
	parser.add_argument('--minimum_images', type=int, default=30, help='Minimum number of images for the account to '
																	   'retain. Tagging of images will only be '
																	   'performed if there are more than this number '
																	   'of images in the account.')
	parser.add_argument('--operation_mode', type=str, default='tag_only',
						help='Sets the operation mode. Options are tag_only, lifecycle_only or tag_and_lifecycle')
	parser.add_argument('--primary_account_role', type=str, required=True, help='IAM role for the primary AWS account '
																				'being targetted by the script. This '
																				'account houses the AMIs and will be '
																				'what tagging and lifecycling will be '
																				'performed against.')
	args = parser.parse_args()

	logging.basicConfig()
	logger = logging.getLogger()
	logger.setLevel(args.log_level)

	# Create boto3 sessions for each role that was passed in and compile them into a list to be passed on.
	all_roles = [args.primary_account_role] + args.additional_account_roles
	boto3_sessions = []

	for role in all_roles:
		arn = arnparse(role)
		session_name = arn.resource
		assumed_role = boto3.client('sts').assume_role(
			RoleArn=role,
			RoleSessionName=session_name
		)
		session_name = boto3.session.Session(
			aws_access_key_id=assumed_role['Credentials']['AccessKeyId'],
			aws_secret_access_key=assumed_role['Credentials']['SecretAccessKey'],
			aws_session_token=assumed_role['Credentials']['SessionToken'],
		)
		boto3_sessions.append(session_name)

	# Compile lists of all available and already tagged AMIs in the primary account to be passed on.
	primary_account_ec2_client = boto3_sessions[0].client('ec2', args.image_region)
	all_available_images = primary_account_ec2_client.describe_images(Owners=['self'])['Images']
	already_tagged_images = [image for image in all_available_images if 'Tags' in image for tags in image['Tags'] if
							 'Expires' in tags['Key']]
	already_tagged_images.sort(key=lambda date: datetime.strptime(date['CreationDate'], '%Y-%m-%dT%H:%M:%S.%fZ'))

	# No bool conversion for str, so favor True for safety given the operation is a purge
	if args.dryrun == 'false':
		dryrunbool = False
	else:
		dryrunbool = True

	operation_mode = args.operation_mode
	if operation_mode == "tag_only":
		logger.info('Operation set to tag only. AMI purging will not be performed. Starting the process...')
		tag_eligible_images(all_available_images, already_tagged_images, boto3_sessions, args.consumer_regions, dryrunbool,
							args.excluded_instance_states, args.grace_period, args.minimum_age, args.minimum_images,
							primary_account_ec2_client)
	elif operation_mode == "lifecycle_only":
		logger.info(
			'Operation set to lifecycle only. Tagging of eligible images will not be performed. Starting the '
			'process...')
		lifecycle_ami(all_available_images, already_tagged_images, boto3_sessions, dryrunbool, args.minimum_images,
					  primary_account_ec2_client)
	elif operation_mode == "tag_and_lifecycle":
		logger.info(
			'Operation set to tag and lifecycle. Tagging of eligible images and AMI purging will be performed. '
			'Starting the process...')
		tag_eligible_images(all_available_images, already_tagged_images, boto3_sessions, args.consumer_regions, dryrunbool,
							args.excluded_instance_states, args.grace_period, args.minimum_age, args.minimum_images,
							primary_account_ec2_client)
		lifecycle_ami(all_available_images, already_tagged_images, boto3_sessions, dryrunbool, args.minimum_images,
					  primary_account_ec2_client)
	else:
		logger.info(
			'No operation method was selected. Set OPERATION_METHOD environment variable to tag_only, '
			'lifecycle_only or tag_and_lifecycle.')