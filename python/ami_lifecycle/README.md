# AMI Lifecycler

This script is written to be automated with Jenkins to implement a
lifecycle for Amazon Machine Images (AMI). This is accomplished by
tagging AMIs with an expiration date using variables that are passed in
via command line arguments. Once the expiration date on an image has
passed, the image is eligible to be purged and is purged on the next run
of the script.

## Getting Started

While this script is written with the intention of being run via a
Jenkins job, these instructions will get you a copy of the project up
and running on your local machine for development and testing purposes.

1. Clone this repo and drop into this directory.
2. Review and setup the [Prerequisites](#prerequisites)
3. Run `python3 ami_lifecycle.py` with [arguments](#script-arguments)
   defined.

### Prerequisites

- [Python 3](https://www.python.org) >= v3.7.7
- [arnparse](https://pypi.org/project/arnparse/) >= v0.2.2
- [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html#installation)
  \>= v1.14.20
- An assumable role with the AWS permissions listed below for the
  primary account you want to run the script against.
	- `ec2:CreateTags`
	- `ec2:DeleteSnapshot`
	- `ec2:DeregisterImage`
	- `ec2:DescribeImages`
	- `ec2:DescribeInstances`
	- `autoscaling:DescribeLaunchConfigurations`
- An assumable role with the AWS permissions below for any additional
  accounts using AMIs from the primary account.
	- `ec2:DescribeInstances`
	- `autoscaling:DescribeLaunchConfigurations`
- A deploy key for your Jenkins to access this repo.

#### Environment variables
- JOB_NAME - Comes from the
  [Jenkins Job](https://wiki.jenkins.io/display/JENKINS/Building+a+software+project#Buildingasoftwareproject-belowJenkinsSetEnvironmentVariables)
  by default. Must be set if running locally.

#### Script Arguments
##### Required Arguments
`--primary_account_role` - Single IAM role ARN with the permissions
outlined in the [prerequisites](#prerequisites) for the primary AWS
account being targeted by the script. This account houses the AMIs and
will be what tagging and lifecycling will be performed against. You must
pass in the full ARN for the role.

##### Safety measures
These variables should be used to safely test the process against an
account:  
`--dryrun` - Sets the dry run boolean to show what the script would do
if run without actually doing it. Defaults to `true`.  
`--operation_mode` - Sets the operation mode of the script. Options are
below. Defaults to `tag_only`.
  - `tag_only` - Sets operation mode to only tag eligible images as expired.
  - `lifecycle_only` - Sets operation mode to only lifecycle already
	tagged images that have passed the expiration date set in the Expires
	tag.
  - `tag_and_lifecycle` - Sets operation mode to perform both tagging and
	purging of eligible images.

##### Exclusion criteria
`--additional_account_roles` - Space separated list of additional AWS
role ARNs with the permissions outlined in the
[prerequisites](#prerequisites) for accounts using AMIs from the primary
account. Used to check for instances or launch configurations using AMIs
from the source AWS account to ensure they're excluded from tagging or
purging. You must pass in the full ARN for the roles.  
`--consumer_regions` - A space separated list of regions with instances
and/or launch configurations using AMIs from the primary account.
Defaults to us-east-1.  
`--excluded_instance_states` - List of instance states as arguments that
should be used to exclude tagging an AMI with the expired tag. If an
instance is in one of these states, it's AMI will be excluded from being
tagged with an Expires tag. For all options, see the
`--instance-state-name` filter in the
[boto3 docs](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html?highlight=describe_instance#EC2.Client.describe_instances)
This is used to ensure instances in these states are considered for
exclusion from tagging and lifecycling. Any states not listed here will
*not* be considered for AMI exclusion from tagging and lifecycling.  
Defaults to `pending running shutting-down stopping stopped`.  
`--minimum_age` - Defines how old an image must be in days (from today's
date) before it's tagged with the Expires tag. For example, if set to
10, any image that has a creation date of 10 days or older in the
account and doesn't meet any other exclusion criteria will be tagged.
Defaults to `30`.  
`--minimum_images` - Defines the minimum number of images the account
should retain in the region the script is being run against. For
example, if set to 10, lifecycling will only commence when there are
more than 10 images in the region of the account specified in the
[environment variables](#environment-variables). Defaults to `30`.

##### Additional arguments
`--grace_period` - Defines the date to be applied in the Expires tag
when setting it. This is calculated in days from today based on this
value. For example, if set to 14, all eligible images will be tagged
with the date of 14 days from today. Defaults to 30.  
`--image_region` - Sets the region housing AMIs in the primary account.
Defaults to us-east-1. If AMIs are housed in a different region than the
default, this should be set to the region where AMIs are housed.  
`--log_level` - The log level of the script. Defaults to `20`.

	NOTSET =    0  
	DEBUG =     10  
	INFO =      20  
	WARNING =   30  
	ERROR =     40  
	CRITICAL =  50  
	
## Deployment

This script is intended to be run in a Team Jenkins instance that has
permissions to assume the role(s) defined in the
[`--primary-account_role`](#required-arguments) and
[`--additional_roles`](#exclusion-criteria) arguments. If running
locally, the AWS credentials you're using must also be able to assume
this role. For additional information, review how
[boto3 handles credentials](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html?highlight=credentials).

### Example pipeline script

```groovy
node {
	stage(name: 'Clean') {
		if (fileExists('.git')) {
			sh 'sudo git clean -fxd -e ".jenkins-*/"'
		} else {
			echo 'No .git folder found.'
		}
	}

	stage(name: 'Checkout') {
		sh 'echo "DUMPING ENVIRONMENT..." && env | sort'
		git credentialsId: '<DEPLOY KEY CREDENTIAL ID FROM JENKINS>', url: 'git@gitlab.com:dabigc/sample-work.git', branch: 'develop'
	}

	stage(name: 'Run AMI Lifecycler') {
		sh """
				  cd scripts/AMI_lifecycle
				  echo "Starting AMI lifecycle process..."

				  docker run -v \$(pwd):/tmp -w "/tmp" \
					-e JOB_NAME="\${JOB_NAME}" \
					<YOUR_DOCKERHUB_USERNAME>/ami-lifecycle \
					/usr/bin/python3 ami_lifecycle.py \
					--additional_roles <ADD ROLE ARNs FOR ADDITIONAL ACCOUNT USING IMAGES FROM THE PRIMARY ACCOUNT. REMOVE IF THERE ARE NO ADDITIONAL ACCOUNTS USING IMAGES FROM THE PRIMARY ACCOUNT.> \
					--consumer_regions us-east-1 \
					--dryrun true \
					--excluded_instance_states pending running shutting-down stopping stopped \
					--grace_period 30 \
					--image_region us-east-1 \
					--log_level 20 \
					--minimum_age 30 \
					--minimum_images 30 \
					--operation_mode tag_and_lifecycle \
					--primary_account_role <ARN FOR ROLE WITH RIGHTS TO THE PRIMARY AWS ACCOUNT HOUSING THE AMIs>
			   """
	}
}
```