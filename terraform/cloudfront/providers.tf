###################################
#### General provider configuration
###################################

# Vault provider that should be used for accessing anything additional outside
# of the original permission set added to the TFE IAM user.
provider "vault" {
  address = "<REDACTED>"
  auth_login {
    namespace = "<REDACTED>"
    path      = "auth/approle/login"
    parameters = {
      role_id   = var.vault_approle_role_id
      secret_id = var.vault_approle_secret_id
    }
  }
}

data "vault_aws_access_credentials" "tfe-admin" {
  backend = "aws"
  role    = var.tfe-admin_iam_role_name
  type    = "sts"
}

provider "aws" {
  region              = "us-east-1"
  allowed_account_ids = ["<REDACTED>"]
  access_key = data.vault_aws_access_credentials.tfe-admin.access_key
  secret_key = data.vault_aws_access_credentials.tfe-admin.secret_key
  token      = data.vault_aws_access_credentials.tfe-admin.security_token
}