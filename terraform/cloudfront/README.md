# CloudFront Distributions Management
## Usage
These modules can be used to create CloudFront distributions in a repeatable manner in an environment using:
- Terraform Enterprise or Terraform Cloud
- Vault for secret management
- AWS IAM roles accessed via Vault

The examples here create the CloudFront distribution and the Route53 records to manage traffic to the distribution.