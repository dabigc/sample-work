data "vault_aws_access_credentials" "public-dns-management" {
  backend = "aws"
  role    = var.public_dns_management_vault_role_name
  type    = "sts"
}

provider "aws" {
  alias      = "public-dns-management"
  access_key = data.vault_aws_access_credentials.public-dns-management.access_key
  secret_key = data.vault_aws_access_credentials.public-dns-management.secret_key
  token      = data.vault_aws_access_credentials.public-dns-management.security_token
}