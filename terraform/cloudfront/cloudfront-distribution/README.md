# Overview
This module is intended to the primary method used by the team to create
CloudFront distributions for properties that we're migrating.

# Assumptions
We assume that you've created an AppRole in Vault for the TFE workspace being
used with this module. That AppRole should have an associated Vault role on the
AWS backend in the corresponding namespace of Vault that provides access to get
STS credentials for an IAM role that provides DNS management capabilities for
the public Route53 zone for the domain that's pointing to this distribution.

# terraform-docs
To keep the configuration details of this module up to date, we're using [terraform-docs](https://terraform-docs.io) for this module. If you modify this module, be sure to `cd` into this module's directory and run the following command to update the section below with your changes.

```bash
docker run --rm --volume "$(pwd):/terraform-docs" -u $(id -u) quay.io/terraform-docs/terraform-docs:0.16.0 markdown /terraform-docs
```

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.1.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.0 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | ~> 3.1.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | n/a |
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.0 |
| <a name="provider_aws.public-dns-management"></a> [aws.public-dns-management](#provider\_aws.public-dns-management) | ~> 3.0 |
| <a name="provider_vault"></a> [vault](#provider\_vault) | ~> 3.1.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_cloudfront"></a> [cloudfront](#module\_cloudfront) | terraform-aws-modules/cloudfront/aws | 2.9.1 |
| <a name="module_lambda-edge-functions"></a> [lambda-edge-functions](#module\_lambda-edge-functions) | terraform-aws-modules/lambda/aws | 2.35.1 |

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.cloudfront_distribution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate) | resource |
| [aws_acm_certificate_validation.cloudfront_distribution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation) | resource |
| [aws_cloudfront_cache_policy.default_cache_behavior](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_cache_policy) | resource |
| [aws_cloudfront_cache_policy.order_cache_behavior](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_cache_policy) | resource |
| [aws_cloudfront_function.cloudfront_distribution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_function) | resource |
| [aws_cloudfront_key_group.cloudfront_key_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_key_group) | resource |
| [aws_cloudfront_origin_request_policy.default_cache_behavior](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_origin_request_policy) | resource |
| [aws_cloudfront_origin_request_policy.order_cache_behavior](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_origin_request_policy) | resource |
| [aws_cloudfront_public_key.cloudfront_public_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_public_key) | resource |
| [aws_route53_record.validation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [archive_file.lambda-edge](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_route53_zone.domain_name](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |
| [vault_aws_access_credentials.public-dns-management](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/aws_access_credentials) | data source |
| [vault_generic_secret.cloudfront_keygroup_secret](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/generic_secret) | data source |

## Inputs

| Name | Description                                                                                                                                                                           | Type | Default | Required |
|------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|---------|:--------:|
| <a name="input_cloudfront_functions"></a> [cloudfront\_functions](#input\_cloudfront\_functions) | Map of functions to be added to the CloudFront distribution's behavior. Most properties have one default function.                                                                    | `map(any)` | n/a | yes |
| <a name="input_comment"></a> [comment](#input\_comment) | Any comments you want to include about the distribution.                                                                                                                              | `string` | `"Managed by Terraform in the cdn-cloudfront-base repo."` | no |
| <a name="input_create_monitoring_subscription"></a> [create\_monitoring\_subscription](#input\_create\_monitoring\_subscription) | If enabled, the resource for the monitoring subscription will be created.                                                                                                             | `bool` | `true` | no |
| <a name="input_create_origin_access_identity"></a> [create\_origin\_access\_identity](#input\_create\_origin\_access\_identity) | Controls if CloudFront origin access identity should be created                                                                                                                       | `bool` | `false` | no |
| <a name="input_create_trusted_key_groups"></a> [create\_trusted\_key\_groups](#input\_create\_trusted\_key\_groups) | Controls if CloudFront trusted key group should be created                                                                                                                            | `bool` | `false` | no |
| <a name="input_custom_error_response"></a> [custom\_error\_response](#input\_custom\_error\_response) | One or more custom error response elements                                                                                                                                            | `any` | `{}` | no |
| <a name="input_default_cache_behavior"></a> [default\_cache\_behavior](#input\_default\_cache\_behavior) | The [default cache behavior](https://registry.terraform.io/modules/terraform-aws-modules/cloudfront/aws/latest#input_default_cache_behavior) for this distribution. To associate a function with this behavior, its key in the functions map _must_ be named default. | `any` | n/a | yes |
| <a name="input_default_cache_policy"></a> [default\_cache\_policy](#input\_default\_cache\_policy) | The default behavior's [cache policy](https://www.terraform.io/docs/providers/aws/r/cloudfront_cache_policy.html) for the distribution.                                               | `any` | `{}` | no |
| <a name="input_default_origin_request_policy"></a> [default\_origin\_request\_policy](#input\_default\_origin\_request\_policy) | The default behavior's [origin request policy](https://www.terraform.io/docs/providers/aws/r/cloudfront_origin_request_policy.html) for the distribution.                             | `any` | `{}` | no |
| <a name="input_default_root_object"></a> [default\_root\_object](#input\_default\_root\_object) | The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL.                                                                       | `string` | `null` | no |
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | Default tags to be used on resources that accept tags within this module.                                                                                                             | `map(string)` | n/a | yes |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | Domain name for the distribution.                                                                                                                                                     | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment for the distribution. Should be either nonprod or prod                                                                                                                | `string` | n/a | yes |
| <a name="input_http_version"></a> [http\_version](#input\_http\_version) | The maximum HTTP version to support on the distribution. Allowed values are http1.1 and http2.                                                                                        | `string` | `"http2"` | no |
| <a name="input_is_ipv6_enabled"></a> [is\_ipv6\_enabled](#input\_is\_ipv6\_enabled) | Whether the IPv6 is enabled for the distribution.                                                                                                                                     | `bool` | `false` | no |
| <a name="input_issue_ssl_certificate"></a> [issue\_ssl\_certificate](#input\_issue\_ssl\_certificate) | Provides a way to disable SSL certificate creation with ACM for this distribution.                                                                                                    | `bool` | `true` | no |
| <a name="input_lambda_edge_functions"></a> [lambda\_edge\_functions](#input\_lambda\_edge\_functions) | Map of Lambda@Edge functions to be added to the CloudFront distribution's behavior.                                                                                                   | `map(any)` | `{}` | no |
| <a name="input_ordered_cache_behavior"></a> [ordered\_cache\_behavior](#input\_ordered\_cache\_behavior) | One or more [ordered\_cache\_behavior](https://registry.terraform.io/modules/terraform-aws-modules/cloudfront/aws/latest#ordered_cache_behavior) for this distribution (multiples allowed). | `any` | `[]` | no |
| <a name="input_origin_access_identities"></a> [origin\_access\_identities](#input\_origin\_access\_identities) | CloudFront origin access identifier                                                                                                                                                   | `string` | `""` | no |
| <a name="input_origins"></a> [origins](#input\_origins) | One or more [origins](https://registry.terraform.io/modules/terraform-aws-modules/cloudfront/aws/latest#input_origin) for this distribution (multiples allowed).                      | `any` | n/a | yes |
| <a name="input_price_class"></a> [price\_class](#input\_price\_class) | The price class for this distribution. One of PriceClass\_All, PriceClass\_200, PriceClass\_100                                                                                       | `string` | `"PriceClass_100"` | no |
| <a name="input_public_dns_management_vault_role_name"></a> [public\_dns\_management\_vault\_role\_name](#input\_public\_dns\_management\_vault\_role\_name) | The Vault role name in the AWS secret backend that's assumable by the AppRole for this TFE workspace. Used to look up data and add records in the Route53 zone for this distribution. | `string` | n/a | yes |
| <a name="input_r53_zone_domain"></a> [r53\_zone\_domain](#input\_r53\_zone\_domain) | Zone name to use if zone is different from domain\_name                                                                                                                               | `any` | `null` | no |
| <a name="input_retain_on_delete"></a> [retain\_on\_delete](#input\_retain\_on\_delete) | Disables the distribution instead of deleting it when destroying the resource through Terraform. If this is set, the distribution needs to be deleted manually afterwards.            | `bool` | `false` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | The stage of the distribution. Generally one of dev, int, cert or prod                                                                                                                | `string` | n/a | yes |
| <a name="input_standard_logging_config"></a> [standard\_logging\_config](#input\_standard\_logging\_config) | Can be used to override the default logging in <REDACTED>-logging account. When used, an exception should be outlined in the PR.                                                      | `any` | `{}` | no |
| <a name="input_standard_logging_enabled"></a> [standard\_logging\_enabled](#input\_standard\_logging\_enabled) | Whether standard logging to S3 should be enabled.                                                                                                                                     | `bool` | `true` | no |
| <a name="input_subdomains"></a> [subdomains](#input\_subdomains) | List of subdomains that will be pointed at the distribution once cutover. Used to construct the ACM certificate SAN list and CloudFront aliases.                                      | `list(string)` | n/a | yes |
| <a name="input_viewer_certificate_minimum_protocol_version"></a> [viewer\_certificate\_minimum\_protocol\_version](#input\_viewer\_certificate\_minimum\_protocol\_version) | The minimum version of the SSL protocol that you want CloudFront to use for HTTPS connections. See all possible values in [this table](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/secure-connections-supported-viewer-protocols-ciphers.html) under "Security policy." | `string` | n/a | yes |
| <a name="input_viewer_certificate_ssl_support_method"></a> [viewer\_certificate\_ssl\_support\_method](#input\_viewer\_certificate\_ssl\_support\_method) | Specifies how you want CloudFront to serve HTTPS requests. One of vip or sni-only. NOTE: vip causes CloudFront to use a dedicated IP address and may incur extra charges.             | `string` | `"sni-only"` | no |
| <a name="input_wait_for_deployment"></a> [wait\_for\_deployment](#input\_wait\_for\_deployment) | If enabled, the resource will wait for the distribution status to change from InProgress to Deployed. Setting this to false will skip the process.                                    | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_distribution_domain_name"></a> [distribution\_domain\_name](#output\_distribution\_domain\_name) | n/a |
<!-- END_TF_DOCS -->    