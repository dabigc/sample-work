output "distribution_domain_name" {
  value = module.cloudfront[var.domain_name].cloudfront_distribution_domain_name
}

output "distribution_id" {
  value = module.cloudfront[var.domain_name].cloudfront_distribution_id
}