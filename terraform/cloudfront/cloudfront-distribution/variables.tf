locals {
  sans_aliases = [for subdomain in var.subdomains : "${subdomain}.${var.domain_name}"]
  default_tags = merge(var.default_tags, {
    "cf-distribution-name" = var.domain_name
    "stage"                = var.stage
  })

  # Create distinct list of domains and SANs that ensures no wildcards
  distinct_domain_names = distinct(
    [for s in concat([var.domain_name], local.sans_aliases) : replace(s, "*.", "")]
  )

  # Get the list of distinct domain_validation_options, with wildcard
  # domain names replaced by the domain name
  validation_domains = var.issue_ssl_certificate ? distinct(
    [for k, v in aws_acm_certificate.cloudfront_distribution[0].domain_validation_options : merge(
      tomap(v), { domain_name = replace(v.domain_name, "*.", "") }
    )]
  ) : []

  zone_domain_name = var.r53_zone_domain == null ? var.domain_name : var.r53_zone_domain
}

variable "public_dns_management_vault_role_name" {
  description = "The Vault role name in the AWS secret backend that's assumable by the AppRole for this TFE workspace. Used to look up data and add records in the Route53 zone for this distribution."
  type        = string
}

variable "issue_ssl_certificate" {
  description = "Provides a way to disable SSL certificate creation with ACM for this distribution."
  type        = bool
  default     = true
}

variable "domain_name" {
  description = "Domain name for the distribution."
  type        = string
}

variable "r53_zone_domain" {
  description = "Zone name to use if zone is different from domain_name"
  default     = null
}

variable "default_tags" {
  description = "Default tags to be used on resources that accept tags within this module."
  type        = map(string)
}

variable "subdomains" {
  description = "List of subdomains that will be pointed at the distribution once cutover. Used to construct the ACM certificate SAN list and CloudFront aliases."
  type        = list(string)
}

variable "cloudfront_functions" {
  description = "Map of functions to be added to the CloudFront distribution's behavior. Most properties have one default function."
  type        = map(any)
}

variable "lambda_edge_functions" {
  description = "Map of Lambda@Edge functions to be added to the CloudFront distribution's behavior."
  type        = map(any)
  default     = {}
}

variable "comment" {
  description = "Any comments you want to include about the distribution."
  type        = string
  default     = "Managed by Terraform in the cdn-cloudfront-base repo."
}

variable "http_version" {
  description = "The maximum HTTP version to support on the distribution. Allowed values are http1.1 and http2."
  type        = string
  default     = "http2"
}

variable "is_ipv6_enabled" {
  description = "Whether the IPv6 is enabled for the distribution."
  type        = bool
  default     = false
}

variable "price_class" {
  description = "The price class for this distribution. One of PriceClass_All, PriceClass_200, PriceClass_100"
  type        = string
  default     = "PriceClass_100"
}

variable "retain_on_delete" {
  description = "Disables the distribution instead of deleting it when destroying the resource through Terraform. If this is set, the distribution needs to be deleted manually afterwards."
  type        = bool
  default     = false
}
variable "wait_for_deployment" {
  description = "If enabled, the resource will wait for the distribution status to change from InProgress to Deployed. Setting this to false will skip the process."
  type        = bool
  default     = true
}
variable "create_monitoring_subscription" {
  description = "If enabled, the resource for the monitoring subscription will be created."
  type        = bool
  default     = true
}
variable "origins" {
  description = "One or more [origins](https://registry.terraform.io/modules/terraform-aws-modules/cloudfront/aws/latest#input_origin) for this distribution (multiples allowed)."
  type        = any
}
variable "default_cache_behavior" {
  description = "The [default cache behavior](https://registry.terraform.io/modules/terraform-aws-modules/cloudfront/aws/latest#input_default_cache_behavior) for this distribution. To associate a function with this behavior, its key in the functions map _must_ be named default."
  type        = any
}
variable "viewer_certificate_minimum_protocol_version" {
  description = "The minimum version of the SSL protocol that you want CloudFront to use for HTTPS connections. See all possible values in [this table](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/secure-connections-supported-viewer-protocols-ciphers.html) under \"Security policy.\""
  type        = string
}
variable "viewer_certificate_ssl_support_method" {
  description = "Specifies how you want CloudFront to serve HTTPS requests. One of vip or sni-only. NOTE: vip causes CloudFront to use a dedicated IP address and may incur extra charges."
  type        = string
  default     = "sni-only"
}
variable "standard_logging_enabled" {
  description = "Whether standard logging to S3 should be enabled."
  type        = bool
  default     = true
}
variable "standard_logging_config" {
  description = "Can be used to override the default logging in <REDACTED>-logging account. When used, an exception should be outlined in the PR."
  type        = any
  default     = {}
}
variable "default_origin_request_policy" {
  description = "The default behavior's [origin request policy](https://www.terraform.io/docs/providers/aws/r/cloudfront_origin_request_policy.html) for the distribution."
  type        = any
  default     = {}
}

variable "default_cache_policy" {
  description = "The default behavior's [cache policy](https://www.terraform.io/docs/providers/aws/r/cloudfront_cache_policy.html) for the distribution."
  type        = any
  default     = {}
}

variable "stage" {
  description = "The stage of the distribution. Generally one of dev, int, cert or prod"
  type        = string
}

variable "origin_access_identities" {
  description = "CloudFront origin access identifier"
  type        = map(any)
  default     = {}
}

variable "create_origin_access_identity" {
  description = "Controls if CloudFront origin access identity should be created"
  default     = false
  type        = bool
}

variable "ordered_cache_behavior" {
  description = "One or more [ordered_cache_behavior](https://registry.terraform.io/modules/terraform-aws-modules/cloudfront/aws/latest#ordered_cache_behavior) for this distribution (multiples allowed)."
  type        = any
  default     = []
}

variable "default_root_object" {
  description = "The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL."
  type        = string
  default     = null
}

variable "custom_error_response" {
  description = "One or more custom error response elements"
  type        = any
  default     = {}
}

variable "environment" {
  description = "The environment for the distribution. Should be either nonprod or prod"
  type        = string
}

variable "create_trusted_key_groups" {
  description = "Controls if CloudFront trusted key group should be created"
  default     = false
  type        = bool
}