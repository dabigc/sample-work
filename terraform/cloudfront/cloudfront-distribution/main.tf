######
# ACM
######

data "aws_route53_zone" "domain_name" {
  provider     = aws.public-dns-management
  name         = local.zone_domain_name
  private_zone = false
}

resource "aws_acm_certificate" "cloudfront_distribution" {
  count = var.issue_ssl_certificate == true ? 1 : 0

  domain_name               = var.domain_name
  subject_alternative_names = length(var.subdomains) == 0 ? [] : local.sans_aliases # main domain should not be in SAN list, otherwise there will be a perpetual plan change
  validation_method         = "DNS"
  tags                      = local.default_tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "validation" {
  provider = aws.public-dns-management
  count    = var.issue_ssl_certificate == false ? 0 : length(local.distinct_domain_names)

  zone_id = data.aws_route53_zone.domain_name.id
  name    = local.validation_domains[count.index]["resource_record_name"]
  type    = local.validation_domains[count.index]["resource_record_type"]
  ttl     = 60

  records = [
    local.validation_domains[count.index]["resource_record_value"]
  ]

  allow_overwrite = true

  depends_on = [
    aws_acm_certificate.cloudfront_distribution
  ]

}

resource "aws_acm_certificate_validation" "cloudfront_distribution" {
  count = var.issue_ssl_certificate ? 1 : 0

  certificate_arn         = aws_acm_certificate.cloudfront_distribution[0].arn
  validation_record_fqdns = aws_route53_record.validation.*.fqdn
}

#############
# Cloudfront
#############

resource "aws_cloudfront_function" "cloudfront_distribution" {
  for_each = var.cloudfront_functions

  name    = join("-", [replace(var.domain_name, ".", "-"), each.key])
  runtime = "cloudfront-js-1.0"
  code    = each.value.function_code

  lifecycle {
    ignore_changes = [
      comment
    ]
  }
}

# Lambda

data "archive_file" "lambda-edge" {
  for_each = var.lambda_edge_functions

  type        = "zip"
  source_dir  = each.value.source_dir
  output_path = "../../functions/distributions/builds/${var.domain_name}/${each.key}.zip"
}

module "lambda-edge-functions" {
  for_each = var.lambda_edge_functions

  source  = "terraform-aws-modules/lambda/aws"
  version = "2.35.1"

  create_role                   = true
  create_package                = false
  publish                       = true
  lambda_at_edge                = true
  attach_cloudwatch_logs_policy = true # Default
  ignore_source_code_hash       = try(each.value.ignore_source_code_hash, false)

  handler = each.value.handler
  runtime = each.value.runtime

  local_existing_package = data.archive_file.lambda-edge[each.key].output_path
  function_name          = "${replace(var.domain_name, ".", "-")}-lambda-${each.key}"

  attach_policy_statements = try(length(each.value.policy_statements) != 0, false)
  policy_statements        = try(each.value.policy_statements, {})

  tags = local.default_tags
}

resource "aws_cloudfront_origin_request_policy" "default_cache_behavior" {
  for_each = var.default_origin_request_policy == {} ? [] : toset([var.domain_name])

  name    = join("-", [replace(var.domain_name, ".", "-"), "default"])
  comment = lookup(var.default_origin_request_policy, "comment", "Managed by Terraform in the cdn-cloudfront-base repo.")

  cookies_config {
    cookie_behavior = var.default_origin_request_policy["cookie_behavior"]

    # Cookies are required if the behavior is in use https://www.terraform.io/docs/providers/aws/r/cloudfront_origin_request_policy.html#items
    dynamic "cookies" {
      for_each = var.default_origin_request_policy["cookie_behavior"] == "whitelist" ? [1] : []

      content {
        items = var.default_origin_request_policy["cookie_items"]
      }
    }
  }

  headers_config {
    header_behavior = var.default_origin_request_policy["header_behavior"]

    # Headers are required if the behavior is in use https://www.terraform.io/docs/providers/aws/r/cloudfront_origin_request_policy.html#items
    dynamic "headers" {
      for_each = var.default_origin_request_policy["header_behavior"] == "whitelist" ? [1] : []

      content {
        items = var.default_origin_request_policy["header_items"]
      }
    }
  }

  query_strings_config {
    query_string_behavior = var.default_origin_request_policy["query_string_behavior"]

    # Query strings are required if the behavior is in use https://www.terraform.io/docs/providers/aws/r/cloudfront_origin_request_policy.html#items
    dynamic "query_strings" {
      for_each = var.default_origin_request_policy["query_string_behavior"] == "whitelist" ? [1] : []

      content {
        items = var.default_origin_request_policy["query_string_items"]
      }
    }
  }
}

resource "aws_cloudfront_origin_request_policy" "order_cache_behavior" {
  for_each = {
    for k, v in var.ordered_cache_behavior : k => v
    if contains(keys(v), "origin_request_policy")
  }

  name    = join("-", [replace(var.domain_name, ".", "-"), var.stage, lookup(each.value["origin_request_policy"], "name", "ordered"), index(var.ordered_cache_behavior, each.value)])
  comment = lookup(each.value["origin_request_policy"], "comment", "Managed by Terraform in the cdn-cloudfront-base repo.")

  cookies_config {
    cookie_behavior = each.value["origin_request_policy"]["cookie_behavior"]
    dynamic "cookies" {
      for_each = each.value["origin_request_policy"]["cookie_behavior"] == "whitelist" ? [1] : []

      content {
        items = each.value["origin_request_policy"]["cookie_items"]
      }
    }
  }
  headers_config {
    header_behavior = each.value["origin_request_policy"]["header_behavior"]
    dynamic "headers" {
      for_each = each.value["origin_request_policy"]["header_behavior"] == "whitelist" ? [1] : []

      content {
        items = each.value["origin_request_policy"]["header_items"]
      }
    }
  }
  query_strings_config {
    query_string_behavior = each.value["origin_request_policy"]["query_string_behavior"]
    dynamic "query_strings" {
      for_each = each.value["origin_request_policy"]["query_string_behavior"] == "whitelist" ? [1] : []

      content {
        items = each.value["origin_request_policy"]["query_string_items"]
      }
    }
  }
}

resource "aws_cloudfront_cache_policy" "default_cache_behavior" {
  for_each = var.default_cache_policy == {} ? [] : toset([var.domain_name])

  name        = join("-", [replace(var.domain_name, ".", "-"), "default"])
  comment     = lookup(var.default_cache_policy, "comment", "Managed by Terraform in the cdn-cloudfront-base repo.")
  default_ttl = lookup(var.default_cache_policy, "default_ttl", null)
  max_ttl     = lookup(var.default_cache_policy, "max_ttl", null)
  min_ttl     = var.default_cache_policy["min_ttl"]

  # Since [parameters_in_cache_key_and_forwarded_to_origin config is
  # optional](https://www.terraform.io/docs/providers/aws/r/cloudfront_cache_policy.html#parameters_in_cache_key_and_forwarded_to_origin),
  # we're not yet adding it as the logic would be somewhat complex to define
  # without a working model.
  parameters_in_cache_key_and_forwarded_to_origin {
    enable_accept_encoding_brotli = lookup(var.default_cache_policy, "enable_accept_encoding_brotli", "false")
    enable_accept_encoding_gzip   = lookup(var.default_cache_policy, "enable_accept_encoding_gzip", "false")

    cookies_config {
      cookie_behavior = lookup(var.default_cache_policy, "cookie_behavior", "none")
      dynamic cookies {
        for_each = length(lookup(var.default_cache_policy, "cookies", "")) == 0 ? [] : [true]
        content {
          items = lookup(var.default_cache_policy, "cookies")
        }
      }
    }
    headers_config {
      header_behavior = lookup(var.default_cache_policy, "header_behavior", "none")
      dynamic headers {
        for_each = length(lookup(var.default_cache_policy, "headers", "")) == 0 ? [] : [true]
        content {
          items = lookup(var.default_cache_policy, "headers")
        }
      }
    }
    query_strings_config {
      query_string_behavior = lookup(var.default_cache_policy, "query_string_behavior", "none")
      dynamic query_strings {
        for_each = length(lookup(var.default_cache_policy, "query_strings", "")) == 0 ? [] : [true]
        content {
          items = lookup(var.default_cache_policy, "query_strings")
        }
      }
    }
  }
}

resource "aws_cloudfront_cache_policy" "order_cache_behavior" {
  for_each = {
    for k, v in var.ordered_cache_behavior : k => v
    if contains(keys(v), "cache_policy")
  }

  name        = join("-", [replace(var.domain_name, ".", "-"), var.stage, lookup(each.value["cache_policy"], "name", "default")])
  comment     = lookup(each.value["cache_policy"], "comment", "Managed by Terraform in the cdn-cloudfront-base repo.")
  default_ttl = lookup(each.value["cache_policy"], "default_ttl", null)
  max_ttl     = lookup(each.value["cache_policy"], "max_ttl", null)
  min_ttl     = lookup(each.value["cache_policy"], "min_ttl", null)


  parameters_in_cache_key_and_forwarded_to_origin {
    enable_accept_encoding_brotli = try(each.value["cache_policy"]["enable_accept_encoding_brotli"], "false")
    enable_accept_encoding_gzip   = try(each.value["cache_policy"]["enable_accept_encoding_gzip"], "false")

    cookies_config {
      cookie_behavior = try(each.value["cache_policy"]["cookie_behavior"], "none")
      dynamic "cookies" {
        for_each = length(try(each.value["cache_policy"]["cookies"], "")) == 0 ? [] : [true]
        content {
          items = each.value["cache_policy"]["cookies"]
        }
      }
    }
    headers_config {
      header_behavior = try(each.value["cache_policy"]["header_behavior"], "none")
      dynamic "headers" {
        for_each = length(try(each.value["cache_policy"]["headers"], "")) == 0 ? [] : [true]
        content {
          items = each.value["cache_policy"]["headers"]
        }
      }
    }
    query_strings_config {
      query_string_behavior = try(each.value["cache_policy"]["query_string_behavior"], "none")
      dynamic "query_strings" {
        for_each = length(try(each.value["cache_policy"]["query_strings"], "")) == 0 ? [] : [true]
        content {
          items = each.value["cache_policy"]["query_strings"]
        }
      }
    }
  }
}
data "vault_generic_secret" "cloudfront_keygroup_secret" {
  for_each = var.create_trusted_key_groups ? toset([var.domain_name]) : []
  path = "${var.stage}/cdn-cloudfront/keygroups"
}

resource "aws_cloudfront_public_key" "cloudfront_public_key" {
  for_each = var.create_trusted_key_groups ? toset([var.domain_name]) : []
  encoded_key = data.vault_generic_secret.cloudfront_keygroup_secret[each.key].data["${join("_", [replace(var.domain_name, ".", "-"), "public_key"])}"]
  lifecycle {
    ignore_changes = all
  }
}
resource "aws_cloudfront_key_group" "cloudfront_key_group" {
  for_each = var.create_trusted_key_groups ? toset([var.domain_name]) : []
  items                          = [aws_cloudfront_public_key.cloudfront_public_key[each.key].id]
  # key_group name is configured to check if the "stage." is included in the domain_name;
  # If not, "stage-" will be added to the name of the key_group;
  # Cloudfront keys are unique per stage, that's why it's important for the names to be stage specific.
  name                           = replace(var.domain_name, "${var.stage}.", "") != var.domain_name ? "${replace(var.domain_name, ".", "-")}_key_group" : "${var.stage}-${replace(var.domain_name, ".", "-")}_key_group"
  depends_on                     = [aws_cloudfront_public_key.cloudfront_public_key]
}

module "cloudfront" {
  source  = "terraform-aws-modules/cloudfront/aws"
  version = "2.9.1"

  for_each = var.origins == {} ? [] : toset([var.domain_name])

  aliases                        = length(var.subdomains) == 0 ? [var.domain_name] : local.sans_aliases
  comment                        = var.comment
  enabled                        = true
  http_version                   = var.http_version
  is_ipv6_enabled                = var.is_ipv6_enabled
  price_class                    = var.price_class
  retain_on_delete               = var.retain_on_delete
  wait_for_deployment            = var.wait_for_deployment
  create_monitoring_subscription = var.create_monitoring_subscription
  tags                           = local.default_tags
  origin                         = var.origins
  default_root_object            = var.default_root_object
  custom_error_response          = var.custom_error_response

  create_origin_access_identity = var.create_origin_access_identity

  origin_access_identities = var.create_origin_access_identity == true ? var.origin_access_identities : { s3_bucket_one: null }

  default_cache_behavior = {
    target_origin_id         = var.default_cache_behavior["target_origin_id"]
    viewer_protocol_policy   = var.default_cache_behavior["viewer_protocol_policy"]
    allowed_methods          = var.default_cache_behavior["allowed_methods"]
    cached_methods           = var.default_cache_behavior["cached_methods"]
    compress                 = var.default_cache_behavior["compress"]
    origin_request_policy_id = length(aws_cloudfront_origin_request_policy.default_cache_behavior) == 0 ? lookup(var.default_cache_behavior, "origin_request_policy_id", null) : aws_cloudfront_origin_request_policy.default_cache_behavior[each.key].id
    cache_policy_id          = length(aws_cloudfront_cache_policy.default_cache_behavior) == 0 ? lookup(var.default_cache_behavior, "cache_policy_id", null) : aws_cloudfront_cache_policy.default_cache_behavior[each.key].id
    use_forwarded_values     = lookup(var.default_cache_behavior, "use_forwarded_values", null)
    trusted_key_groups       = length(aws_cloudfront_key_group.cloudfront_key_group) == 0 ? lookup(var.default_cache_behavior, "trusted_key_groups", null) : [aws_cloudfront_key_group.cloudfront_key_group[each.key].id]

    function_association = !contains(keys(aws_cloudfront_function.cloudfront_distribution), "default") ? {} : {
      "viewer-request" = {
        function_arn = aws_cloudfront_function.cloudfront_distribution["default"].arn
      }
    }

    lambda_function_association = !contains(keys(var.lambda_edge_functions), "default") ? {} : {
      (var.lambda_edge_functions["default"].event_type) = {
        lambda_arn = "${module.lambda-edge-functions["default"].lambda_function_arn}:${try(var.lambda_edge_functions["default"].version, module.lambda-edge-functions["default"].lambda_function_version)}"
      }
    }

  }

  ordered_cache_behavior = [
    for k, v in var.ordered_cache_behavior : {
      path_pattern             = v["path_pattern"]
      target_origin_id         = v["target_origin_id"]
      viewer_protocol_policy   = v["viewer_protocol_policy"]
      allowed_methods          = v["allowed_methods"]
      cached_methods           = v["cached_methods"]
      compress                 = v["compress"]
      use_forwarded_values     = v["use_forwarded_values"]
      cache_policy_id          = try(length(v["cache_policy"]), 0) != 0 ? aws_cloudfront_cache_policy.order_cache_behavior[k].id : v["cache_policy_id"]
      origin_request_policy_id = try(length(v["origin_request_policy"]), 0) != 0 ? aws_cloudfront_origin_request_policy.order_cache_behavior[k].id : try(v["origin_request_policy_id"], null)
      trusted_key_groups       = length(aws_cloudfront_key_group.cloudfront_key_group) == 0 ? try(v["trusted_key_groups"], null) : "${aws_cloudfront_key_group.cloudfront_key_group[each.key].id}"

      function_association = try(length(v["function_association"].viewer_request), 0) == 0 ? {} : {
        "viewer-request" = {
          function_arn = aws_cloudfront_function.cloudfront_distribution[v["function_association"].viewer_request].arn
        }
      }

      lambda_function_association = try(length(v["lambda_function_association"].origin_response), 0) == 0 ? {} : {
        "origin-response" = {
          lambda_arn = "${module.lambda-edge-functions[v["lambda_function_association"].origin_response].lambda_function_arn}:${try(var.lambda_edge_functions[v["lambda_function_association"].origin_response].version, module.lambda-edge-functions[v["lambda_function_association"].origin_response].lambda_function_version)}"
        }
      }
    }
  ]

  viewer_certificate = {
    acm_certificate_arn            = aws_acm_certificate.cloudfront_distribution[0].arn
    cloudfront_default_certificate = false
    minimum_protocol_version       = var.viewer_certificate_minimum_protocol_version
    ssl_support_method             = var.viewer_certificate_ssl_support_method
  }

  depends_on = [
    # CloudFront needs the certificate to be valid before it can be used with a distribution.
    aws_acm_certificate_validation.cloudfront_distribution[0]
  ]

  # Unless standard logging is disabled, check for a custom config else default
  # to the <REDACTED> account (which should be the norm).
  logging_config = var.standard_logging_enabled == false ? {} : var.standard_logging_config != {} ? var.standard_logging_config : {
    bucket          = "<REDACTED_S3_LOGGING_BUCKET_NAME>-${var.environment}.s3.amazonaws.com"
    include_cookies = false
    prefix          = "${var.environment}/${var.domain_name}"
  }
}