locals {
  domain_name                           = "foo.com"
  public_dns_management_vault_role_name = "foo-com-public-dns-management"
  cf-distribution_vault_role_name       = "foo_prod"
}

data "aws_caller_identity" "cloudfront_distribution" {}

data "aws_cloudfront_cache_policy" "default-managed-cachingdisabled" {
  name = "Managed-CachingDisabled"
}

module "www-foo-com-weighted-cdn-dns-records" {
  source                                = "./cloudfront-route53-records"
  zone_name                             = local.domain_name
  cloudfront_distribution_id            = module.foo-com-cf-distribution.distribution_id
  public_dns_management_vault_role_name = local.public_dns_management_vault_role_name
  cf-distribution_vault_role_name       = local.cf-distribution_vault_role_name

  records = {
    www-cdn-cloudfront = {
      name  = "www-cdn"
      type  = "A"
      alias = { constructed_by_module = true }
    }
  }
}

module "foo-com-cf-distribution" {
  source                                      = "./cloudfront-distribution"
  subdomains                                  = ["www"]
  domain_name                                 = local.domain_name
  public_dns_management_vault_role_name       = local.public_dns_management_vault_role_name
  default_tags                                = local.default_tags
  comment                                     = "foo.com distribution. Managed by Terraform."
  http_version                                = "http2"
  viewer_certificate_minimum_protocol_version = "TLSv1.2_2021"
  environment                                 = local.environment
  stage                                       = local.environment
  cloudfront_functions = {
    default = {
      event_type    = "viewer-request"
      function_code = file("./functions/www.foo.com.js")
    }
  }

  lambda_edge_functions = {
    default = {
      event_type              = "origin-response"
      source_dir              = ".functions/www.foo.com-origin-response"
      handler                 = "www-foo-com-origin-response.handler"
      runtime                 = "nodejs14.x"
      version                 = 1
      ignore_source_code_hash = true
    }
  }

  origins = {
    ("origin-www.foo.com") = {
      domain_name = "origin-www.foo.com"
      custom_origin_config = {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "match-viewer"
        origin_ssl_protocols   = ["TLSv1.2"]
      }
    }
  }

  default_origin_request_policy = {
    cookie_behavior = "whitelist"
    cookie_items = [
      "akamai_edge_id",
      "wordpress*",
      "comment*",
      "wp-settings*"
    ]
    header_behavior = "whitelist"
    header_items = [
      "Host",
      "True-Client-IP",
      "CloudFront-Forwarded-Proto",
      "CloudFront-Is-Mobile-Viewer",
      "CloudFront-Is-Tablet-Viewer",
      "CloudFront-Is-Desktop-Viewer"
    ]
    query_string_behavior = "all"
  }

  default_cache_policy = {
    min_ttl                       = 1
    max_ttl                       = 31536000
    default_ttl                   = 86400
    cookie_behavior               = "none"
    header_behavior               = "none"
    query_string_behavior         = "all"
    enable_accept_encoding_brotli = "true"
    enable_accept_encoding_gzip   = "true"
  }

  default_cache_behavior = {
    target_origin_id       = "origin-www.foo.com"
    viewer_protocol_policy = "allow-all"
    allowed_methods        = ["GET", "HEAD", "OPTIONS", "POST", "PUT", "PATCH", "DELETE"]
    cached_methods       = ["GET", "HEAD"]
    compress             = true
    cache_policy_id      = data.aws_cloudfront_cache_policy.default-managed-cachingoptimized.id
    use_forwarded_values = false
  }
  ordered_cache_behavior = [
    {
      path_pattern           = "/asp_api/*"
      target_origin_id       = "origin-www.foo.com"
      viewer_protocol_policy = "allow-all"

      allowed_methods      = ["GET", "HEAD", "OPTIONS", "POST", "PUT", "PATCH", "DELETE"]
      cached_methods       = ["GET", "HEAD"] #default, but will not be cached
      compress             = true
      use_forwarded_values = false
      cache_policy_id      = data.aws_cloudfront_cache_policy.default-managed-cachingdisabled.id

      origin_request_policy = {
        cookie_behavior = "whitelist"
        cookie_items = [
          "wordpress*",
          "akamai_edge_id"
        ]
        header_behavior = "whitelist"
        header_items = [
          "Host",
          "True-Client-IP"
        ]
        query_string_behavior = "all"
      }

      function_association = {
        viewer_request = "default"
      }
      lambda_function_association = {
        origin_response = "default"
      }
    },
    {
      path_pattern           = "wp-admin/*"
      target_origin_id       = "origin-www.foo.com"
      viewer_protocol_policy = "redirect-to-https"

      allowed_methods      = ["GET", "HEAD", "OPTIONS", "POST", "PUT", "PATCH", "DELETE"]
      cached_methods       = ["GET", "HEAD"]
      compress             = true
      use_forwarded_values = false

      cache_policy_id = data.aws_cloudfront_cache_policy.default-managed-cachingdisabled.id

      origin_request_policy = {
        cookie_behavior       = "all"
        header_behavior       = "allViewer"
        query_string_behavior = "all"
      }

      function_association = {
        viewer_request = "default"
      }
      lambda_function_association = {
        origin_response = "default"
      }
    },
    {
      path_pattern           = "wp-login.php" #Same as wp-admin
      target_origin_id       = "origin-www.foo.com"
      viewer_protocol_policy = "redirect-to-https"

      allowed_methods      = ["GET", "HEAD", "OPTIONS", "POST", "PUT", "PATCH", "DELETE"]
      cached_methods       = ["GET", "HEAD"]
      compress             = true
      use_forwarded_values = false

      cache_policy_id = data.aws_cloudfront_cache_policy.default-managed-cachingdisabled.id

      origin_request_policy = {
        cookie_behavior       = "all"
        header_behavior       = "allViewer"
        query_string_behavior = "all"
      }

      function_association = {
        viewer_request = "default"
      }
      lambda_function_association = {
        origin_response = "default"
      }
    },
    {
      path_pattern           = "wp-*"
      target_origin_id       = "origin-www.foo.com"
      viewer_protocol_policy = "allow-all"

      allowed_methods      = ["GET", "HEAD", "OPTIONS", "POST", "PUT", "PATCH", "DELETE"]
      cached_methods       = ["GET", "HEAD"]
      compress             = true
      use_forwarded_values = false

      cache_policy_id = data.aws_cloudfront_cache_policy.default-managed-cachingdisabled.id

      origin_request_policy = {
        cookie_behavior       = "all"
        header_behavior       = "allViewer"
        query_string_behavior = "all"
      }

      function_association = {
        viewer_request = "default"
      }
      lambda_function_association = {
        origin_response = "default"
      }
    }
  ]
}