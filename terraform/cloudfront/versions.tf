terraform {
  required_version = "1.1.3"
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "foobarinc"
    workspaces {
      name = "cdn-cloudfront-foobar"
    }
  }
  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "3.1.1"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.75.0"
    }
  }
}