data "aws_cloudfront_distribution" "this" {
  provider = aws.for_cf-distribution
  id       = var.cloudfront_distribution_id
}