module "route53_records_weighted" {
  count   = 1
  source  = "terraform-aws-modules/route53/aws//modules/records"
  version = "2.5.0"

  zone_name    = var.zone_name
  private_zone = false

  records = concat(local.records_with_alias, local.records_without_alias)
}