# Overview

This module is used for Route53 record management for CloudFront distributions in one of two ways.

1. Implement an alias record where the module does the heavy lifting by ensuring
   the record is pointing at the distrbution ID passed into the module with the
   `cloudfront_distribution_id` variable. This is the most common use-case for
   this module. 
   - In order to leverage this, ensure the alias key is present in `weighted
     records` input variable and it's value is { constructed_by_module = true }
2. Manually pass in any other record configuration that's specified to the [public Route53
   records
   module](https://registry.terraform.io/modules/terraform-aws-modules/route53/aws/latest/submodules/records)
   requirements as this is the module being used under the hood.

# Assumptions

- All records in the [original_records input variable](#input_original_records)
  contain the configuration of the DNS records that you want to implement. If
  the records are already in use, they need to match what's in the public
  Route53 zone _exactly_.
  - If a value in this map differs from what's currently set, the record _will
    be updated_ to match what's entered in its entry within this map.
  - If a record's value isn't in the `approved_original_cdn_records` local list, the apply will fail with an error like the one below. 
    __This is intentional__ as the only records we should be managing with this
    module are those pointing to CloudFront distributions. If a new address is discovered, it will need
    to be added to the list.

           ```bash
           │ Error: Null value found in list
           │ 
           │   with module.foo-cdn-dns-records.module.route53_records.aws_route53_record.this["www CNAME"],
           │   on .terraform/modules/foo-cdn-dns-records.route53_records/modules/records/main.tf line 39, in resource "aws_route53_record" "this":
           │   39:   records                          = jsondecode(each.value.records)
           │ 
           │ Null values are not allowed for this attribute value.
           ```
       - If a record's name contains one of the values in `local.restricted_cdn_names`, then a plan / apply would  fail with an error such as the one below:

               ```bash
             │ Error: Invalid template interpolation value
             │   on .terraform/modules/foo-weighted-cdn-dns-records.route53_records_weighted/modules/records/main.tf line 17, in locals:
             │   17:     join(" ", compact(["${rs.name} ${rs.type}", lookup(rs, "set_identifier", "")])) => merge(rs, {
             │     ├────────────────
             │     │ rs.name is null
             │ The expression result is null. Cannot include a null value in a string
             │ template.
             ```
- An AppRole has been created in Vault for the TFE workspace being used with this module. That AppRole should have
  associated Vault roles on the AWS backend in the corresponding namespace of
  Vault that provides access to:
    -  Get STS credentials for an IAM role that provides DNS management capabilities for the public Route53 zone for the domain
  that's pointing to this distribution.
    - Get STS credentials for an IAM role that provides permission to describe
      the CloudFront distribution in order to pull data such as the domain name
      for the distribution and use it to perform validation of where the record
      will point and/or construct the alias record.

# Usage

1. Get the current records under Terraform management with the initial module call.
2. Whoever reviews the PR with the current records validates that the record's values match what they're currently set
   to in the public Route53 zone.
3. Merge the first PR to bring the records under Terraform management as they're currently configured.
4. Open a PR with the updates to the record that you want to make. Terraform
   will show the changes.
5. Apply when ready to cutover

# Shortcomings of ROUTE_53

- TTL can't be set to 0, aws_route53_record doesn't allow that. [Issue 11381](https://github.com/hashicorp/terraform-provider-aws/issues/11381)
- TTL can't be updated in subsequent PRs, this will need to be done through API/UI -  [Issue 16389](https://github.com/hashicorp/terraform/issues/16389)


# Module Call Example

```hcl
module "www-cdn-foo-com-weighted-cdn-dns-records" {
  source                                = "./cloudfront-route53-records"
  zone_name                             = local.foo-com.zone_name
  cloudfront_distribution_id            = local.foo-com_distribution_id
  public_dns_management_vault_role_name = local.foo-com.public_dns_management_vault_role_name
  cf-distribution_vault_role_name       = local.cf-distribution_vault-iam_role
  records = {
    www-cdn-cloudfront = {
      name  = "www-cdn.foo"
      type  = "A"
      alias = { constructed_by_module = true }
    }
  }
}
```

<!-- BEGIN_TF_DOCS -->
# terraform-docs
To keep the configuration details of this module up to date, we're using
[terraform-docs](https://terraform-docs.io) for this module. If you modify
this module, be sure to `cd` into this module's directory and run the
following command to update the section below with your changes.

```bash
docker run --rm --volume "$(pwd):/terraform-docs" -u $(id -u) quay.io/terraform-docs/terraform-docs:0.16.0 markdown /terraform-docs
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.1.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.0 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | ~> 3.1.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws.for_cf-distribution"></a> [aws.for\_cf-distribution](#provider\_aws.for\_cf-distribution) | ~> 3.0 |
| <a name="provider_vault"></a> [vault](#provider\_vault) | ~> 3.1.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_route53_records_weighted"></a> [route53\_records\_weighted](#module\_route53\_records\_weighted) | terraform-aws-modules/route53/aws//modules/records | 2.5.0 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudfront_distribution.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/cloudfront_distribution) | data source |
| [vault_aws_access_credentials.cf-distribution](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/aws_access_credentials) | data source |
| [vault_aws_access_credentials.public-dns-management](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/aws_access_credentials) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cf-distribution_vault_role_name"></a> [cf-distribution\_vault\_role\_name](#input\_cf-distribution\_vault\_role\_name) | The Vault role name in the AWS secret backend that's assumable by the AppRole for this TFE workspace. Used to look up details on this CloudFront distribution. | `string` | n/a | yes |
| <a name="input_cloudfront_distribution_id"></a> [cloudfront\_distribution\_id](#input\_cloudfront\_distribution\_id) | The ID for the CloudFront distribution that's being targeted with this module. | `string` | n/a | yes |
| <a name="input_public_dns_management_vault_role_name"></a> [public\_dns\_management\_vault\_role\_name](#input\_public\_dns\_management\_vault\_role\_name) | The Vault role name in the AWS secret backend that's assumable by the AppRole for this TFE workspace. Used to look up data and update records in the Route53 zone for this distribution. | `string` | n/a | yes |
| <a name="input_records"></a> [records](#input\_records) | Map of weighted AWS Route53 DNS records which will hold a weighted\_routing\_policy each, to determine the amount of traffic sent to one or another. Structured to match the [Route53 records submodule](https://github.com/terraform-aws-modules/terraform-aws-route53/blob/v2.5.0/modules/records/main.tf#L31). | `map(any)` | n/a | yes |
| <a name="input_zone_name"></a> [zone\_name](#input\_zone\_name) | The Route53 zone name where the records exist or should be created. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->