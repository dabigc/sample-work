data "vault_aws_access_credentials" "public-dns-management" {
  backend = "aws"
  role    = var.public_dns_management_vault_role_name
  type    = "sts"
}

provider "aws" {
  access_key = data.vault_aws_access_credentials.public-dns-management.access_key
  secret_key = data.vault_aws_access_credentials.public-dns-management.secret_key
  token      = data.vault_aws_access_credentials.public-dns-management.security_token
}

data "vault_aws_access_credentials" "cf-distribution" {
  backend = "aws"
  role    = var.cf-distribution_vault_role_name
  type    = "sts"
}

provider "aws" {
  region     = "us-east-1"
  alias      = "for_cf-distribution"
  access_key = data.vault_aws_access_credentials.cf-distribution.access_key
  secret_key = data.vault_aws_access_credentials.cf-distribution.secret_key
  token      = data.vault_aws_access_credentials.cf-distribution.security_token
}