locals {
  approved_original_cdn_records = [
    "foobar.com.edgekey.net",
    "foo.com.edgekey.net"
  ]

  distribution_domain_name = data.aws_cloudfront_distribution.this.domain_name

  alias_template = {
    name    = data.aws_cloudfront_distribution.this.domain_name
    zone_id = data.aws_cloudfront_distribution.this.hosted_zone_id
  }

  records_with_alias = [for weighted_record in var.records :
    {
      name  = weighted_record.name
      type  = weighted_record.type
      alias = weighted_record.alias == { constructed_by_module = true } ? local.alias_template : weighted_record.alias

      allow_overwrite = true
    } if contains(keys(weighted_record), "alias")
  ]

  records_without_alias = [for weighted_record in var.records :
    {
      name = weighted_record.name
      type = weighted_record.type
      ttl  = weighted_record.ttl == "0" ? "1" : weighted_record.ttl #aws_route53_record creation fails if set to 0

      weighted_routing_policy = lookup(weighted_record, "weighted_routing_policy", {})
      set_identifier          = lookup(weighted_record, "set_identifier", null)

      allow_overwrite = true
      records         = [for record in weighted_record.records : contains(local.approved_original_cdn_records, record) ? record : null]
    } if !contains(keys(weighted_record), "alias")
  ]
}

variable "public_dns_management_vault_role_name" {
  description = "The Vault role name in the AWS secret backend that's assumable by the AppRole for this TFE workspace. Used to look up data and update records in the Route53 zone for this distribution."
  type        = string
}

variable "zone_name" {
  description = "The Route53 zone name where the records exist or should be created."
  type        = string
}

variable "records" {
  description = "Map of weighted AWS Route53 DNS records which will hold a weighted_routing_policy each, to determine the amount of traffic sent to one or another. Structured to match the Route53 records submodule."
  type        = map(any)
}

variable "cloudfront_distribution_id" {
  description = "The ID for the CloudFront distribution that's being targeted with this module."
  type        = string
}

variable "cf-distribution_vault_role_name" {
  description = "The Vault role name in the AWS secret backend that's assumable by the AppRole for this TFE workspace. Used to look up details on this CloudFront distribution."
  type        = string
}