locals {
  prefix      = "bar"
  service     = "content-delivery-network"
  lob         = "web-services"
  environment = "prod"
  name        = join("-", [local.prefix, local.service, local.environment])
  name_tfe    = join("-", [local.prefix, local.service, local.environment, "tfe"])

  default_tags = {
    "lob"      = join(".", ["com", "foo", local.lob])
    "service"  = local.service
    "role"     = "null"
    "shared"   = "false"
  }
}

variable "vault_approle_role_id" {
  description = "The AppRole role-id used for the Vault provider."
  type        = string
  sensitive   = true
}

variable "vault_approle_secret_id" {
  description = "The AppRole secret-id used for the Vault provider."
  type        = string
  sensitive   = true
}

variable "tfe-admin_iam_role_name" {
  description = "The IAM role name to be used by the Vault provider to interact with this AWS account."
  type        = string
}