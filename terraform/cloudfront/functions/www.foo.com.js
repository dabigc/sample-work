function handler(event) {
    var request = event.request;

    // Mimicks the [Allow POST
    // Requests](https://control.akamai.com/wh/CUSTOMER/AKAMAI/en-US/WEBHELP/property-manager/property-manager-help/GUID-7B253193-009B-42E8-ADFB-4F49707E4157.html)
    // behavior at Akamai. By default, CloudFront will allow a POST request to the
    // origin without a content-length header being passed with it.
    if (request.method == "POST" && !request.headers['content-length']) {
        var response = {
            statusCode: 411,
            statusDescription: 'Length Required'
        }
        return response;
    }

    // Mimicks the Send True Client IP Header behavior in the Origin Server
    // configuration at Akamai
    var clientIP = event.viewer.ip;
    //Add the true-client-ip header to the incoming request
    request.headers['true-client-ip'] = {value: clientIP};

    //Do not gzip if old browsers
    if (request.headers['user-agent']){
        var userAgent = request.headers['user-agent'].value;
        if (userAgent.includes('Mozilla/4') == 1 || userAgent.includes('MSIE 4') == 1 || userAgent.includes('MSIE 5') == 1){
            request.headers['accept-encoding'] = {value: 'none'};
        }
    }

    // Mimicks Akamai Edge Server Identification functionality
    request.cookies['akamai_edge_id'] = {value: 'fooapproved'};

    return request;
}