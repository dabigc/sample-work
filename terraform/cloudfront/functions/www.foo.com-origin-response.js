exports.handler = async (event, context) => {
    const request = event.Records[0].cf.request;
    const response = event.Records[0].cf.response;
    const respHeaders = response.headers;
    const cacheControlHeader = 'cache-control';
    const contentLength = 'content-length';
    const contentType = 'content-type';

    const match_fileExtensions = request.uri.match(/(.*\.css|js|aif|aiff|au|avi|bin|bmp|cab|carb|cct|cdf|class|doc|dcr|dtd|ext|flv|gcf|gff|grv|hdml|hqx|ico|ini|jgp|jpeg|mov|mp3|nc|pct|pdf|png|ppc|pws|swa|swf|txt|vbs|w32|wav|wbmp|wml|wmlc|wmls|wmlsc|xsd|zip|pict|tif|tiff|mid|midi|ttf|eot|woff|woff2|otf|svg|svgz|webp|jxr|jar|jp2)$/)

    if (typeof respHeaders[cacheControlHeader] !=   'undefined') {
        if(match_fileExtensions) {
            respHeaders[cacheControlHeader] = [{
                key: cacheControlHeader,
                value: 'max-age=86400',
            }];
        } else {
            respHeaders[cacheControlHeader] = [{
                key: cacheControlHeader,
                value: 'no-store',
            }];
        }
    }

    if (typeof respHeaders[contentLength] !=   'undefined' & typeof respHeaders[contentType] !=   'undefined'){
        if (respHeaders[contentLength][0].value == 0 & respHeaders[contentType][0].value.startsWith('text/css')){
                respHeaders[cacheControlHeader] = [{
                    key: cacheControlHeader,
                    value: 'no-store',
                }];
        }
    }

    return response;