# Required Variables
variable "cost" {
  description = "Cost tag value used for billing"
}

variable "environment" {
  description = "Abbreviated environment value used for component tagging"
}

variable "region" {
  description = "The AWS region VPC is hosted in"
}

variable "vpc_cidr" {
  description = "Subnet CIDR of AWS VPC"
}

variable "vpc_dns_support" {
  description = "Enable DNS support for VPC"
  default     = true
}

variable "vpc_dns_hostnames" {
  description = "Enable DNS hostnames for VPC"
  default     = true
}

variable "vpc_tags" {
  description = "Additional tags for the VPC"
  default     = {}
}

variable "sagemaker_subnet_ids" {
  type        = list(string)
  description = "Triggers VPC endpoint creations for SageMaker. Subnet IDs that interfaces should be created in for SageMaker VPC endpoints."
  default     = []
}

# Local Variables
locals {
  base_tags = {
	environment = var.environment
	cost        = var.cost
  }

  base_shared_resource_tags = {
	environment = replace(lookup(local.base_tags, "environment"), "/-(dev|int|cert|ecert|qa|prod|transient)$/", "")
	cost        = replace(lookup(local.base_tags, "cost"), "/-(dev|int|cert|ecert|qa|prod|transient)$/", "")
  }
}