resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = var.vpc_dns_hostnames
  enable_dns_support   = var.vpc_dns_support

  tags = merge(local.base_tags, { "Name" = var.environment }, var.vpc_tags)
}

resource "aws_security_group" "sagemaker_vpc_endpoint" {
  for_each    = length(var.sagemaker_subnet_ids) > 0 ? toset(["1"]) : []
  name        = "${var.environment}-sagemaker-vpc-endpoint"
  description = "Allows access for the VPC and other SGs to access SageMaker APIs via its VPC endpoint."
  vpc_id      = aws_vpc.main.id
  tags = merge(
	local.base_tags,
	var.vpc_tags,
	{ "Name" = "${var.environment}-sagemaker-vpc-endpoint" }
  )

  ingress {
	description = "TLS from VPC"
	from_port   = 443
	to_port     = 443
	protocol    = "tcp"
	cidr_blocks = [aws_vpc.main.cidr_block]
  }
}

# Opted to use the public module as you won't be able to plan successfully if
# you don't pass required attributes in like you can with direct resource
# creation. The intent is we add future VPC endpoints to this same module call
# as we can conditionally control each endpoint in the "endpoints" input
# variable with if logic as was done for SageMaker.
module "vpc_endpoints" {
  source   = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  version  = ">= 3.19.0"
  for_each = length(var.sagemaker_subnet_ids) > 0 ? toset(["1"]) : []

  vpc_id = aws_vpc.main.id
  tags   = merge(local.base_tags, var.vpc_tags)

  endpoints = merge({
	for sagemaker_endpoint_service_name in [
	  "aws.sagemaker.${var.region}.notebook",
	  "aws.sagemaker.${var.region}.studio",
	  "com.amazonaws.${var.region}.sagemaker.api",
	  "com.amazonaws.${var.region}.sagemaker.featurestore-runtime",
	  "com.amazonaws.${var.region}.sagemaker.runtime",
	  "com.amazonaws.${var.region}.sagemaker.runtime-fips"
	  # Metrics endpoint is limited to 3 AZs per account and looks to be random.
	  # Leads to apply errors so commenting out for now.
	  #"com.amazonaws.${var.region}.sagemaker.metrics",
	] :
	sagemaker_endpoint_service_name => {
	  service_name       = sagemaker_endpoint_service_name
	  security_group_ids = [aws_security_group.sagemaker_vpc_endpoint[each.key].id]
	  subnet_ids         = var.sagemaker_subnet_ids
	  tags               = { "Name" = "${var.environment}-sagemaker-${regex("[^.]+$", sagemaker_endpoint_service_name)}" }
	} if length(var.sagemaker_subnet_ids) > 0
	},
	{
	  # Example of adding additional VPC endpoints in the future using this module.  
	  # s3 = {
	  #   service = "s3"
	  #   tags    = { Name = "s3-vpc-endpoint" }
	  # }
	}
  )
}