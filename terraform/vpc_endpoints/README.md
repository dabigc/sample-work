# VPC Endpoint Implementation
This module has more functionality to implement all aspects of a 
VPC (NAT gateway, NACLs, etc). I've pared it down to a recent
improvement I made to implement VPC endpoints in the overarching 
VPC module.

After initially attempting to use direct resource creation and 
finding differences with configurations (interface vs gateway)
and different service names for SageMaker, I decided the public
AWS module made more sense as it leverages a data object to 
build the attributes of the VPC endpoint and, importantly, fails
out at _plan_ time instead of _apply_ time post-merge.

The intent (as with everything I design) was to leave it 
flexible enough to add other VPC endpoints as the public module 
is designed to have all endpoint configurations passed in as a map. 